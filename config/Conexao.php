<?php
class Conexao {

    private static $instancia;
    private $banco;

    private function Conexao() {
        $this->banco = new mysqli('localhost', 'root', '','aula_rest');
    }

    public static function obterInstancia() {
        if(self::$instancia == null){
            self::$instancia = new Conexao();
        }
        return self::$instancia;
    }

    public function obterConexao() {
        return $this->banco;
    }
}
