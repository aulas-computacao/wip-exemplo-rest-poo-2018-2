<?php
interface DAOInterface {
    public function listar();
    public function buscaPorId($id);
    public function salvar($objeto);
    public function atualizar($objeto);
    public function remover($id);
}
