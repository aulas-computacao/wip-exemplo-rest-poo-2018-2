<?php
class Partido {
    private $id;
    private $nome;
    private $sigla;
    private $numero;

    public function getId() { return $this->id; }
    public function getNome() { return $this->nome; }
    public function getSigla() { return $this->sigla; }
    public function getNumero() { return $this->numero; }

    public function setId($id) { $this->id = $id; }
    public function setNome($nome) { $this->nome = $nome; }
    public function setSigla($sigla) { $this->sigla = $sigla; }
    public function setNumero($numero) { $this->numero = $numero; }
}
