<?php
require_once "interfaces/DAOInterface.php";
require_once "config/Conexao.php";
class PartidoDAO implements DAOInterface {

    private $conexao;
    public function PartidoDAO() {
        $this->conexao = Conexao::obterInstancia();
    }

    public function listar(){
        $sql = "SELECT * FROM partido";
        $resultado = $this->conexao->obterConexao()->query($sql);
        $retorno = [];
        if($resultado->num_rows > 0){
            $retorno = $resultado->fetch_all(MYSQLI_ASSOC);
        }
        return $retorno;
    }   

    public function buscaPorId($id) {}
    public function salvar($objeto) {}
    public function atualizar($objeto){}
    public function remover($id){}
}
