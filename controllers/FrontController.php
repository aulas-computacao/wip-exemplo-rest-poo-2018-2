<?php
require_once "models/PartidoDAO.php";
class FrontController {

    public function __construct() {
        // $param = $_GET['param'];
        // echo $param;
        $dao = new PartidoDAO();
        $partidos = $dao->listar();
        echo json_encode($partidos);
    }

}
